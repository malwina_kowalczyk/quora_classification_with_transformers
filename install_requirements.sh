conda install pytorch cudatoolkit=10.1 -c pytorch 
pip install tokenizers==0.8.1.rc1
pip install simpletransformers==0.45.0
pip install wandb 
# pip install gcsfs when working with Colab

git clone -q https://github.com/NVIDIA/apex
cd apex
pip install -q -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./